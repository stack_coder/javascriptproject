import fs from "fs";

const saveUser = (userdb) => {
  const path = "text.txt";
  const promise = new Promise((resolve, reject) => {
    fs.writeFile(path, userdb, { flag: "a" }, (err) => {
      if (err) {
        console.log(err.message);
        reject();
      }
      console.log("data is written");
      resolve(true);
    });
  });
  return promise;
};

export default saveUser;
