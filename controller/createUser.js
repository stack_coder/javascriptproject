import { v4 as uuidv4 } from "uuid";
import saveUser from "./saveUser.js";
import { validUser } from "./validUser.js";

const createUser = async (req, res) => {
  const id = uuidv4();

  const {
    name,
    bloodGroup,
    weight,
    height,
    dateOfBirth,
    email,
    phoneNo,
    postalCode,
  } = req.body;

  console.log(req.body);

  const userdb = {
    bloodGroup,
    weight,
    height,
    dateOfBirth,
    email,
    phoneNo,
    postalCode,
    name,
    id,
  };
  console.log(userdb);

  const valid = validUser(userdb);
  if (valid) {
    const str = JSON.stringify({ ...userdb });
    const result = await saveUser(str);
    console.log({ result });
    if (result) {
      res.json(userdb);
    }
  }
};

export { createUser };
