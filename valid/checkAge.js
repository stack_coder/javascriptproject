const getAge = (born, now) => {
  const birthday = new Date(now.getFullYear(), born.getMonth(), born.getDate());
  if (now >= birthday) return now.getFullYear() - born.getFullYear();
  return now.getFullYear() - born.getFullYear() - 1;
};

const ConvertDateToAge = (dateOfBirth) => {
  console.log(dateOfBirth);
  const now = new Date();
  const birthdate = dateOfBirth.split("/");
  const born = new Date(birthdate[2], birthdate[1] - 1, birthdate[0]);
  const age = getAge(born, now);

  if (age <= 18) {
    console.log("Input Error - Age should be greater then or equal to 18");
    return false;
  }

  return age;
};

export { ConvertDateToAge };
