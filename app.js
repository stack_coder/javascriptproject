import dotenv from "dotenv";
import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import User from "./routes/users.js";
import Data from "./routes/data.js";

dotenv.config();
const app = express();

app.use(express.json());
app.use(cors());

app.use(bodyParser.json());

const port = process.env.PORT || 3000;

app.use("/", Data);
app.use("/", User);

app.listen(port, () => {
  console.log(`server is started on port ${port}`);
});
