import express from "express";
import { data } from "../controller/data.js";

const router = express.Router();

router.get("/", data);

export default router;
